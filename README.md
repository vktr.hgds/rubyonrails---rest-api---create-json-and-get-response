This is a little application made in Ruby on Rails.<br>
db: sqlite<br>
There are 2 projects.<br>
The first is a server, which creates a JSON response from the URL parameter.<br><br>

localhost:3000?id=2<br>
if the user with the id exists, then the json response contains the following fields:<br>
name, email, gender, age<br>
<br>
if it doesn't exist, then an 'error' field will be the response with the next msg:<br>
"User doesn't exist in the db"<br>
<br>
The second application uses this api, and saves the 4 fields into a db table called Person<br>
Person(name, email, gender, age)<br>
