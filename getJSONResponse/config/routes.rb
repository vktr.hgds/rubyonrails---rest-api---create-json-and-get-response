Rails.application.routes.draw do

  resources :people do
     # this is the way to add a custom to a controller: open on the following url: /people/users
    get 'users', on: :collection
  end
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html

  # this how to add custom route
  # get "users", to: "poeple/users"
  root to: "people#index"

end
