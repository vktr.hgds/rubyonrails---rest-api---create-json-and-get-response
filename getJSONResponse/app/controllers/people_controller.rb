require 'net/http'
require 'json'

class PeopleController < ApplicationController

  def index
    @msg = ""

    id = params[:search]
    uri = URI("http://localhost:3000/?id=#{id}")
    response = Net::HTTP.get_response(uri)
    @data = JSON.parse(response&.body)

    if @data['error']
      @data.each do |key, value|
         @msg += "#{key}: #{value}"
      end
    else
      @new_person = Person.create(name: @data['name'], email: @data['email'], gender: @data['gender'], age: @data['age'].to_i)
      @msg = "Successfully added to the db."
    end
  end


  def users
    @msg = []

    gender_type = params[:gender]
    uri = URI("http://localhost:3000/users/gender?value=#{gender_type}")
    response = Net::HTTP.get_response(uri)
    @data = JSON.parse(response&.body)

    @data.length.times do |i|
      if @data[i]['error']
        @msg << "Error, enter the following words into the input box: Male/Female"
        puts @data[0]['error']
        break
      else
        @msg << "#{@data[i]['users']['name']} - #{@data[i]['users']['email']} - #{@data[i]['users']['gender']} - #{@data[i]['users']['age'].to_i}\n"
      end
    end

  end

end
