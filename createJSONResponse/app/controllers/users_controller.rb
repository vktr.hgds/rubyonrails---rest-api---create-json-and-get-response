class UsersController < ApplicationController

  def index
    #how to use: http://localhost:3000/?id=2
    user = User.find_by(id: params[:id])
    output = ""

    if user

      # if all data needed, then use the following format (easy way)
      # @output = user.to_json
      # else, if you only need specific data from a User record, then the following format is better
      # this version doesn't contain the username & password fields

      output = {
          :name => user.full_name,
          :email => user.email,
          :gender => user.gender,
          :age => user.age
      }

    else
      output = { :error => "User doesn't exist in the db." }
    end
    render :json => output
  end

  #create response with several json objects according to their gender type
  def gender
    users = User.where("gender = :value", value: params[:value])
    output = []

    if users.length > 0
      users.each do |user|
          tmp = { :users =>
                  {
                    :name => user.full_name,
                    :email => user.email,
                    :gender => user.gender,
                    :age => user.age
                  }
          }
          output << tmp
      end
    else
      output << { :error => "No users found. Valid gender values: Male/Female" }
    end

    output = output.to_json
    render :json => output
  end

end
